#RPIWhere
python脚本，安装完后，每次开机自动运行，告知自己的ip。有4种可选通知方式。

1.udp广播，通过发送udp广播内容“iamrpi”，在同网段内的主机监听8712端口可以获得树莓派的ip地址。

2.tcp请求，向固定的某个ip发起tcp连接请求，并发送“iamrpi”内容。

3.web请求，向设定的某个网址发起post，post内容携带ip地址。

4.通过yeelink，记录ip地址。

##安装方法
`sudo python RPIWhere.py install`
可以安装1个或者多个通知方式。

##卸载
`sudo python RPIWhere.py remove`

##测试
`python RPIWhere.py test`

##主机监听
###当使用udp广播或者tcp请求时，需要设置主机为服务端进行来自树莓派的消息。
`sudo python RPIWhere.py udplisten`

`sudo python RPIWhere.py tcplisten`
##Yeelink
使用yeelink的话，请创建泛型传感器