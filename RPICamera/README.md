**树莓派摄像头相关模块**

***fragment.py***

安装后开机自行启动，并定时拍摄一张图片保存在本地，如果有使用新浪云存储的话，还会自动上传一张小尺寸的图片到新浪云存储上备份。

***fragment.conf***

配置文件

[setting]

`pic-size-small`:拍摄的照片的小尺寸

`pic-size-big`:拍摄照片的大尺寸

[sae]

`accessKey`：新浪云存储的accessKey

`secretKey`:新浪云存储的secretKey

`bucket`：新浪云存储的bucket名字


***安装***

1.编辑fragment.conf配置文件。如果不使用新浪云存储的话值请留空即可。

2.`python fragment.py install`

3.`reboot`

***卸载***

`python fragment.py remove`

***使用***

`python fragment.py`